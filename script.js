const clipboardEl = document.getElementById('clipboardBtn');

clipboardEl.addEventListener('click', () => {
    const textarea = document.createElement('textarea')
    const result = document.getElementById('clipboard').value;

    if(!result) { return }

    textarea.value = result
    document.body.appendChild(textarea)
    textarea.select()
    document.execCommand('copy')
    textarea.remove()
    alert('text copied to clipboard!')
});